﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    [Header("Enemy Health and Damage")]
    private float enemyHealth = 120f;
    private float presentHealth;
    public float giveDamage = 5f;
    public float enemySpeed;

    [Header("Enemy Things")]
    public NavMeshAgent enemyAgent;
    public Transform lookPoint;
    public GameObject ShootingRaycastArea;
    public Transform playerBody;
    public LayerMask PlayerLayer;
    public Transform Spawn;
    public Transform EnemyCharacter;

    [Header("Enemy Shooting Var")]
    public float timebtwShoot;
    bool previouslyShoot;

    [Header("Enemy Animation and Spark effect")]
    public Animator anim;
    public ParticleSystem muzzleSpark;

    [Header("Enemy States")]
    public float visionRadius; // bán kính tầm nhìn
    public float shootingRadius; // bán kính bắn
    public bool playerInvisionRadius; // player trong bán kính tầm nhìn
    public bool playerInshootingRadius; // player trong bán kính bắn
    public bool isPlayer = false;

    public ScoreManager scoreManager;

    [Header("Sound Effects")]
    public AudioSource audioSource;
    public AudioClip shootingSound;

    private void Awake()
    {
        enemyAgent = GetComponent<NavMeshAgent>();
        presentHealth = enemyHealth;
    }

    private void Update()
    {
        //  kiểm tra xem người chơi có nằm trong bán kính tầm nhìn của kẻ địch hay không
        // transform.position - bắt đầu kiểm tra từ vị trí của enemy
        // visionRadius - đại diện cho bán kính của quả cầu kiểm tra. Kẻ địch sẽ chỉ nhìn thấy người chơi nếu người chơi nằm trong quả cầu này
        // PlayerLayer - Chỉ các đối tượng trên layer (LayerMask) này mới được kiểm tra xem có nằm trong quả cầu hay không
        playerInvisionRadius = Physics.CheckSphere(transform.position, visionRadius, PlayerLayer);
        playerInshootingRadius = Physics.CheckSphere(transform.position, shootingRadius, PlayerLayer);

        if (playerInvisionRadius && !playerInshootingRadius) Pursueplayer();
        if (playerInvisionRadius && playerInshootingRadius) ShootPlayer();
    }

    
    private void Pursueplayer()
    {
        if(enemyAgent.SetDestination(playerBody.position)) // tìm đến vị trí player
        {
            anim.SetBool("Running", true);
            anim.SetBool("Shooting", false);
        }

        else
        {
            anim.SetBool("Running", false);
            anim.SetBool("Shooting", false);
        }
    }

    private void ShootPlayer()
    {
        // đối tượng kẻ thù được chỉ định để di chuyển đến vị trí hiện tại của chính nó
        // đảm bảo rằng hành vi di chuyển của đối tượng kẻ thù luôn được cập nhật, ngay cả khi vị trí của nó không thay đổi
        enemyAgent.SetDestination(transform.position);

        transform.LookAt(lookPoint);

        if(!previouslyShoot)
        {
            muzzleSpark.Play();
            audioSource.PlayOneShot(shootingSound);

            RaycastHit hit;
            if (Physics.Raycast(ShootingRaycastArea.transform.position, ShootingRaycastArea.transform.forward, out hit, shootingRadius))
            {
                Debug.Log("Shooting" + hit.transform.name);

                if(isPlayer == true)
                {
                    PlayerMovement playerBody = hit.transform.GetComponent<PlayerMovement>();
                    if (playerBody != null)
                    {
                        playerBody.playerHitDamage(giveDamage);
                    }
                }

                else
                {
                    PlayerAI playerBody = hit.transform.GetComponent<PlayerAI>();
                    if (playerBody != null)
                    {
                        playerBody.PlayerAIHitDamage(giveDamage);
                    }
                }

                anim.SetBool("Running", false);
                anim.SetBool("Shooting", true);
            }

            previouslyShoot = true;
            Invoke(nameof(ActiveShooting), timebtwShoot);
        }
    }

    private void ActiveShooting()
    {
        previouslyShoot = false;
    }

    public void enemyHitDamage(float takeDamage)
    {
        presentHealth -= takeDamage;

        if (presentHealth <= 0f)
        {
            StartCoroutine(Respawn());
        }
    }

    IEnumerator Respawn()
    {
        enemyAgent.SetDestination(transform.position);
        enemySpeed = 0f;
        shootingRadius = 0f;
        visionRadius = 0f;
        playerInvisionRadius = false;
        playerInshootingRadius = false;
        anim.SetBool("Die", true);
        anim.SetBool("Running", false);
        anim.SetBool("Shooting", false);

        // animations

        Debug.Log("Dead");
        scoreManager.enemyKills += 1;

        // khi enemy bị chết thì sẽ tắt CapsuleCollider để lúc có bắn vào thì sẽ không tăng chỉ số điểm (kills)
        gameObject.GetComponent<CapsuleCollider>().enabled = false;
        yield return new WaitForSeconds(5f);

        Debug.Log("Spawn");

        // Khi enemy sống lại thì lại bật CapsuleCollider lên
        gameObject.GetComponent<CapsuleCollider>().enabled = true;

        presentHealth = 120f;
        enemySpeed = 1f;
        shootingRadius = 10f;
        visionRadius = 100f;
        playerInvisionRadius = true;
        playerInshootingRadius = false;

        // animations
        anim.SetBool("Die", false);
        anim.SetBool("Running", true);

        // spawn point
        EnemyCharacter.transform.position = Spawn.transform.position;
        Pursueplayer();
    }
}
