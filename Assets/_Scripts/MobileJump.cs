﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobileJump : MonoBehaviour
{
    public Animator animator;
    public PlayerMovement player;

    public void OnPointerDown()
    {
        if(player.onSurface && player.currentPlayerSpeed > 0f)
        {
            animator.SetBool("Walk", false);
            animator.SetBool("isJump", true);
            player.velocity.y = Mathf.Sqrt(player.jumpRange * -2 * player.gravity);
        }

        else if (player.onSurface) // nếu player đang đứng trên mặt đất thì mới nhảy được
        {
            animator.SetBool("Walk", false);
            animator.SetBool("isJump", true);
            player.velocity.y = Mathf.Sqrt(player.jumpRange * -2 * player.gravity);

            Debug.Log("Aaaaaaaaaaaaaaa");
        }

        else
        {
            //animator.ResetTrigger("Jump"); // reset lại để không nhảy đi nhảy lại
            animator.SetBool("isJump", false);
        }
    }

    public void OnPointerUp()
    {
        //animator.ResetTrigger("Jump"); // reset lại để không nhảy đi nhảy lại
        animator.SetBool("isJump", false);
    }
}
