﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [Header("Player Health Things")]
    private float playerHealth = 1000f;
    private float presentHealth;
    public HealthBar healthBar;

    [Header("Player Movement")]
    public float playerSpeed = 1.9f;
    public float currentPlayerSpeed = 0f;
    public float playerSprint = 3f;
    public float currentPlayerSprint = 0f;

    [Header("Player Camera")]
    public Transform playerCamera;

    [Header("Player Animator and Gravity")]
    public CharacterController cC;
    public float gravity = -9.81f;
    public Animator animator;

    [Header("Player jumping & velocity")]
    public float jumpRange = 1f;
    public float turnCalmTime = 0.1f;
    float turnCalmVelocity;
    public Vector3 velocity;
    public Transform surfaceCheck;
    public bool onSurface;
    public float surfaceDistance = 0.4f;
    public LayerMask surfaceMask;

    public bool mobileInputs;
    public FixedJoystick joystick;
    public FixedJoystick Sprintjoystick;

    private void Start()
    {
        //Cursor.lockState = CursorLockMode.Locked; // khóa con trỏ

        presentHealth = playerHealth;
        healthBar.GiveFullHealth(playerHealth);
    }

    void Update()
    {
        if(currentPlayerSpeed > 0)
        {
            Sprintjoystick = null;
        }
        else
        {
            FixedJoystick sprintJS = GameObject.Find("PlayerSprintJoystick").GetComponent<FixedJoystick>();
            Sprintjoystick = sprintJS;
        }

        // check vị trí bề mặt, khoảng cách bề mặt, mặt nạ bề mặt
        onSurface = Physics.CheckSphere(surfaceCheck.position, surfaceDistance, surfaceMask);
        if(onSurface && velocity.y < 0)
        {
            velocity.y -= 2f;
        }
        // Gravity
        velocity.y += gravity * Time.deltaTime;
        cC.Move(velocity * Time.deltaTime);

        playerMovement();

        Jump();

        PlayerSprint();
    }

    void playerMovement()
    {
        if(mobileInputs == true)
        {
            float horizontal_axis = joystick.Horizontal;
            float vertical_axis = joystick.Vertical;

            // Muốn di chuyển thì phải cần biết Character đang di chuyển ở đâu nên cần dùng Vector3
            Vector3 direction = new Vector3(horizontal_axis, 0f, vertical_axis).normalized; // cần phải được chuẩn hóa nên dùng normalized

            // magnitude - cường độ
            // Tức là nếu người chơi đang di chuyển theo Character thì sẽ di chuyển player về hướng đó
            if (direction.magnitude >= 0.1f)
            {
                animator.SetBool("Walk", true);
                animator.SetBool("Running", false);
                animator.SetBool("Idle", false);
                animator.SetTrigger("Jump");
                animator.SetBool("AimWalk", false);
                animator.SetBool("IdleAim", false);

                // quay + di chuyển Player theo hướng di chuyển tương ứng với các phím (W, S, A, D)

                // Rad2Deg chuyển thành độ
                float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + playerCamera.eulerAngles.y; // xoay nhân vật theo góc Camera
                                                                                                                        // dần thay đổi một góc đã cho theo độ về phía góc mục tiêu mong muốn theo vận tốc + thời gian
                float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnCalmVelocity, turnCalmTime);
                transform.rotation = Quaternion.Euler(0f, angle, 0f); // Quaternion.Euler() - trả về 1 phép quay:
                                                                      // trục z quanh trục z
                                                                      // trục x quanh trục x
                                                                      // trục y quanh trục y

                Vector3 moveDirection = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward; // xoay nhân vật theo trục y (theo hướng forward) camera
                cC.Move(moveDirection.normalized * playerSpeed * Time.deltaTime);

                currentPlayerSpeed = playerSpeed; // khi cố gắng di chuyển player thì tốc độ hiện tại = tốc độ player
            }

            else
            {
                animator.SetBool("Running", false);
                animator.SetBool("Idle", true);
                animator.SetTrigger("Jump");
                animator.SetBool("Walk", false);
                animator.SetBool("AimWalk", false);
                currentPlayerSpeed = 0f;
            }
        }

        else
        {
            float horizontal_axis = Input.GetAxisRaw("Horizontal");
            float vertical_axis = Input.GetAxisRaw("Vertical");

            // Muốn di chuyển thì phải cần biết Character đang di chuyển ở đâu nên cần dùng Vector3
            Vector3 direction = new Vector3(horizontal_axis, 0f, vertical_axis).normalized; // cần phải được chuẩn hóa nên dùng normalized

            // magnitude - cường độ
            // Tức là nếu người chơi đang di chuyển theo Character thì sẽ di chuyển player về hướng đó
            if (direction.magnitude >= 0.1f)
            {
                animator.SetBool("Walk", true);
                animator.SetBool("Running", false);
                animator.SetBool("Idle", false);
                animator.SetTrigger("Jump");
                animator.SetBool("AimWalk", false);
                animator.SetBool("IdleAim", false);

                // quay + di chuyển Player theo hướng di chuyển tương ứng với các phím (W, S, A, D)

                // Rad2Deg chuyển thành độ
                float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + playerCamera.eulerAngles.y; // xoay nhân vật theo góc Camera
                                                                                                                        // dần thay đổi một góc đã cho theo độ về phía góc mục tiêu mong muốn theo vận tốc + thời gian
                float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnCalmVelocity, turnCalmTime);
                transform.rotation = Quaternion.Euler(0f, angle, 0f); // Quaternion.Euler() - trả về 1 phép quay:
                                                                      // trục z quanh trục z
                                                                      // trục x quanh trục x
                                                                      // trục y quanh trục y

                Vector3 moveDirection = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward; // xoay nhân vật theo trục y (theo hướng forward) camera
                cC.Move(moveDirection.normalized * playerSpeed * Time.deltaTime);

                currentPlayerSpeed = playerSpeed; // khi cố gắng di chuyển player thì tốc độ hiện tại = tốc độ player
            }

            else
            {
                animator.SetBool("Running", false);
                animator.SetBool("Idle", true);
                animator.SetTrigger("Jump");
                animator.SetBool("Walk", false);
                animator.SetBool("AimWalk", false);
                currentPlayerSpeed = 0f;
            }
        }
    }

    public void Jump()
    {
        if(Input.GetButtonDown("Jump") && onSurface) // nếu nhấn nút nhảy và đồng thời player đang đứng trên mặt đất thì mới nhảy được
        {
            animator.SetBool("Walk", false);
            animator.SetTrigger("Jump");
            //animator.SetBool("isJump", true);
            velocity.y = Mathf.Sqrt(jumpRange * -2 * gravity);
        }
        else
        {
            animator.ResetTrigger("Jump"); // reset lại để không nhảy đi nhảy lại
            //animator.SetBool("isJump", false);
        }
    }

    void PlayerSprint()
    {
        if (mobileInputs == true)
        {
            float horizontal_axis = Sprintjoystick.Horizontal;
            float vertical_axis = Sprintjoystick.Vertical;

            // Muốn di chuyển thì phải cần biết Character đang di chuyển ở đâu nên cần dùng Vector3
            Vector3 direction = new Vector3(horizontal_axis, 0f, vertical_axis).normalized; // cần phải được chuẩn hóa nên dùng normalized

            // magnitude - cường độ
            // Tức là nếu người chơi đang di chuyển theo Character thì sẽ di chuyển player về hướng đó
            if (direction.magnitude >= 0.1f)
            {
                animator.SetBool("Running", true);
                animator.SetBool("Idle", false);
                animator.SetBool("Walk", false);
                animator.SetBool("IdleAim", false);

                // quay + di chuyển Player theo hướng di chuyển tương ứng với các phím (W, S, A, D)

                // Rad2Deg chuyển thành độ
                float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + playerCamera.eulerAngles.y; // xoay nhân vật theo góc Camera
                                                                                                                        // dần thay đổi một góc đã cho theo độ về phía góc mục tiêu mong muốn theo vận tốc + thời gian
                float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnCalmVelocity, turnCalmTime);
                transform.rotation = Quaternion.Euler(0f, angle, 0f); // Quaternion.Euler() - trả về 1 phép quay:
                                                                      // trục z quanh trục z
                                                                      // trục x quanh trục x
                                                                      // trục y quanh trục y

                Vector3 moveDirection = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward; // xoay nhân vật theo trục y (theo hướng forward) camera
                cC.Move(moveDirection.normalized * playerSprint * Time.deltaTime);

                currentPlayerSprint = playerSprint; // khi cố gắng di chuyển player thì tốc độ hiện tại = tốc độ player
            }

            else
            {
                animator.SetBool("Idle", false);
                animator.SetBool("Walk", false);
                currentPlayerSprint = 0f;
            }
        }

        else
        {
            float horizontal_axis = Input.GetAxisRaw("Horizontal");
            float vertical_axis = Input.GetAxisRaw("Vertical");

            // Muốn di chuyển thì phải cần biết Character đang di chuyển ở đâu nên cần dùng Vector3
            Vector3 direction = new Vector3(horizontal_axis, 0f, vertical_axis).normalized; // cần phải được chuẩn hóa nên dùng normalized

            // magnitude - cường độ
            // Tức là nếu người chơi đang di chuyển theo Character thì sẽ di chuyển player về hướng đó
            if (direction.magnitude >= 0.1f)
            {
                animator.SetBool("Running", true);
                animator.SetBool("Idle", false);
                animator.SetBool("Walk", false);
                animator.SetBool("IdleAim", false);

                // quay + di chuyển Player theo hướng di chuyển tương ứng với các phím (W, S, A, D)

                // Rad2Deg chuyển thành độ
                float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + playerCamera.eulerAngles.y; // xoay nhân vật theo góc Camera
                                                                                                                        // dần thay đổi một góc đã cho theo độ về phía góc mục tiêu mong muốn theo vận tốc + thời gian
                float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnCalmVelocity, turnCalmTime);
                transform.rotation = Quaternion.Euler(0f, angle, 0f); // Quaternion.Euler() - trả về 1 phép quay:
                                                                      // trục z quanh trục z
                                                                      // trục x quanh trục x
                                                                      // trục y quanh trục y

                Vector3 moveDirection = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward; // xoay nhân vật theo trục y (theo hướng forward) camera
                cC.Move(moveDirection.normalized * playerSprint * Time.deltaTime);

                currentPlayerSprint = playerSprint; // khi cố gắng di chuyển player thì tốc độ hiện tại = tốc độ player
            }

            else
            {
                animator.SetBool("Idle", false);
                animator.SetBool("Walk", false);
                currentPlayerSprint = 0f;
            }
        }
    }

    public void playerHitDamage(float takeDamage)
    {
        presentHealth -= takeDamage;
        healthBar.SetHealth(presentHealth);

        if(presentHealth <= 0f)
        {
            PlayerDie();
        }
    }

    private void PlayerDie()
    {
        Cursor.lockState = CursorLockMode.None; // mở khóa con trỏ

        Destroy(gameObject);
    }
}
