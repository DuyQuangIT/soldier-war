﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioMenu : MonoBehaviour
{
    public AudioSource audioSource;
    public AudioClip[] audioClips; // Khai báo mảng AudioClip

    public AudioClip clip1;
    public AudioClip clip2;
    public AudioClip clip3;
    public AudioClip clip4;

    void Start()
    {
        // Gán các AudioClip vào mảng audioClips
        audioClips = new AudioClip[] { clip1, clip2, clip3, clip4 };

        PlayRandomClip(); // để chạy random 1 AudioClip mỗi khi nhấn chạy chương trình
    }

    void PlayRandomClip()
    {
        // Phát ngẫu nhiên một AudioClip trong mảng audioClips
        int randomIndex = Random.Range(0, audioClips.Length);
        audioSource.PlayOneShot(audioClips[randomIndex]); // phát ngẫu nhiên 1 bài tại 1 vị trí trong list
    }
}
