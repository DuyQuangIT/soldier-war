using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public AudioSource audioSource;
    public AudioClip audioClip;

    public void Play()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

        audioSource.clip = audioClip;
        audioSource.Play();
    }

    public void Exit()
    {
        Application.Quit();
        audioSource.clip = audioClip;
        audioSource.Play();
    }
}
