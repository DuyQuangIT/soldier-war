﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreManager : MonoBehaviour
{
    [Header("Score Manager")]
    public int kills;
    public int enemyKills;
    public Text playerKillCounter;
    public Text enemyKillCounter;
    public Text Maintext;

    // Khi chơi trên mobile, mỗi khi khởi động lại thì điểm sẽ về lại 0
    private void Awake()
    {
        if(PlayerPrefs.HasKey("kills"))
        {
            kills = PlayerPrefs.GetInt("0");
        }

        if (PlayerPrefs.HasKey("enemyKills"))
        {
            enemyKills = PlayerPrefs.GetInt("0");
        }
    }

    private void Update()
    {
        StartCoroutine(WinOrLose());
    }

    IEnumerator WinOrLose()
    {
        playerKillCounter.text = "" + kills;
        enemyKillCounter.text = "" + enemyKills;

        if(kills >= 10)
        {
            Maintext.text = "Blue Team Victory";
            PlayerPrefs.SetInt("kills", kills); // PlayerPrefs lớp có sẵn
            Time.timeScale = 0f; // Khi timeScale bằng 0, trò chơi sẽ tạm dừng và không có sự tiến triển của thời gian.
            // sau khi đặt thời gian tạm dừng thành 0, dòng lệnh này cho phép trò chơi dừng lại trong 5 giây trước khi tiếp tục.

            yield return new WaitForSeconds(5f); // Dòng lệnh này tạm dừng việc thực thi tiếp theo trong coroutine trong 5 giây

            SceneManager.LoadScene("Menu");
        }

        else if(enemyKills >= 10)
        {
            Maintext.text = "Red Team Victory";
            PlayerPrefs.SetInt("enemyKills", enemyKills); // PlayerPrefs lớp có sẵn
            Time.timeScale = 0f; // Khi timeScale bằng 0, trò chơi sẽ tạm dừng và không có sự tiến triển của thời gian.
            // sau khi đặt thời gian tạm dừng thành 0, dòng lệnh này cho phép trò chơi dừng lại trong 5 giây trước khi tiếp tục.

            yield return new WaitForSeconds(5f); // Dòng lệnh này tạm dừng việc thực thi tiếp theo trong coroutine trong 5 giây

            SceneManager.LoadScene("Menu");

            
        }
    }
}
