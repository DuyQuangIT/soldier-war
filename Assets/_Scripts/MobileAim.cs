﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MobileAim : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public Animator animator;
    private bool isAim = false; // kiểm tra xem đã nhấn vào cái nút ngắm hay chưa

    public MobileAim mobileAim;
    public PlayerMovement player;

    public GameObject AimCam;
    public GameObject AimCanvas;
    public GameObject ThirdPersonCam;
    public GameObject ThirdPersonCanvas;

    void Start()
    {
        mobileAim.enabled = false;
    }

    void Update()
    {
        if (isAim == true)
        {
            animator.SetBool("Idle", false);
            animator.SetBool("IdleAim", true);
            animator.SetBool("AimWalk", false);
            animator.SetBool("Walk", false);

            // (tác dụng là zoom camera lên và bật cái tâm ngắm nhỏ đi)
            ThirdPersonCam.SetActive(false);
            AimCam.SetActive(true);
            AimCanvas.SetActive(true);
        }

        else if(isAim == true && player.currentPlayerSpeed > 0f)
        {
            animator.SetBool("Idle", false);
            animator.SetBool("IdleAim", true);
            animator.SetBool("AimWalk", true);
            animator.SetBool("Walk", true);

            // (tác dụng là zoom camera lên và bật cái tâm ngắm nhỏ đi)
            ThirdPersonCam.SetActive(false);
            ThirdPersonCanvas.SetActive(false);
            AimCam.SetActive(true);
            AimCanvas.SetActive(true);
        }

        else
        {
            animator.SetBool("Idle", true);
            animator.SetBool("IdleAim", false);
            animator.SetBool("AimWalk", false);

            ThirdPersonCam.SetActive(true);
            ThirdPersonCanvas.SetActive(true);
            AimCam.SetActive(false);
            AimCanvas.SetActive(false);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        // Khi nút button được nhấn giữ
        isAim = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        // Khi nút button được nhả ra
        isAim = false;
    }
}
