﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MobieShoot : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public Animator animator;

    public Rifle rifle;

    private bool isHoldingButton = false; // Biến xác định xem nút button có đang được nhấn giữ hay không

    void Update()
    {
        if (isHoldingButton == true)
        {
            if (Time.time >= rifle.nextTimeToShoot)
            {
                animator.SetBool("Fire", true);
                animator.SetBool("Idle", false);
                rifle.nextTimeToShoot = Time.time + 1f / rifle.fireCharge;

                rifle.Shoot();
            }
        }

        else
        {
            animator.SetBool("Fire", false);
            animator.SetBool("Idle", true);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        // Khi nút button được nhấn giữ
        isHoldingButton = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        // Khi nút button được nhả ra
        isHoldingButton = false;
    }
}
