﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerAI : MonoBehaviour
{
    [Header("Player Health and Damage")]
    private float PlayerHealth = 120f;
    private float presentHealth;
    public float giveDamage = 5f;
    public float PlayerSpeed;

    [Header("Player Things")]
    public NavMeshAgent PlayerAgent;
    public Transform lookPoint;
    public GameObject ShootingRaycastArea;
    public Transform enemyBody;
    public LayerMask enemyLayer;
    public Transform Spawn;
    public Transform PlayerCharacter;

    [Header("Player Shooting Var")]
    public float timebtwShoot;
    bool previouslyShoot;

    [Header("Player Animation and Spark effect")]
    public Animator anim;
    public ParticleSystem muzzleSpark;

    [Header("Player States")]
    public float visionRadius; // bán kính tầm nhìn
    public float shootingRadius; // bán kính bắn
    public bool enemyInvisionRadius; // enemy trong bán kính tầm nhìn
    public bool enemyInshootingRadius; // enemy trong bán kính bắn

    public ScoreManager scoreManager;

    [Header("Sound Effects")]
    public AudioSource audioSource;
    public AudioClip shootingSound;

    private void Awake()
    {
        PlayerAgent = GetComponent<NavMeshAgent>();
        presentHealth = PlayerHealth;
    }

    private void Update()
    {
        //  kiểm tra xem người chơi có nằm trong bán kính tầm nhìn của kẻ địch hay không
        // transform.position - bắt đầu kiểm tra từ vị trí của Player
        // visionRadius - đại diện cho bán kính của quả cầu kiểm tra. Kẻ địch sẽ chỉ nhìn thấy người chơi nếu người chơi nằm trong quả cầu này
        // PlayerLayer - Chỉ các đối tượng trên layer (LayerMask) này mới được kiểm tra xem có nằm trong quả cầu hay không
        enemyInvisionRadius = Physics.CheckSphere(transform.position, visionRadius, enemyLayer);
        enemyInshootingRadius = Physics.CheckSphere(transform.position, shootingRadius, enemyLayer);

        if (enemyInvisionRadius && !enemyInshootingRadius) PursueEnemy();
        if (enemyInvisionRadius && enemyInshootingRadius) ShootEnemy();
    }


    private void PursueEnemy()
    {
        if (PlayerAgent.SetDestination(enemyBody.position)) // tìm đến vị trí enemy
        {
            anim.SetBool("Running", true);
            anim.SetBool("Shooting", false);
        }

        else
        {
            anim.SetBool("Running", false);
            anim.SetBool("Shooting", false);
        }
    }

    private void ShootEnemy()
    {
        // đối tượng kẻ thù được chỉ định để di chuyển đến vị trí hiện tại của chính nó
        // đảm bảo rằng hành vi di chuyển của đối tượng kẻ thù luôn được cập nhật, ngay cả khi vị trí của nó không thay đổi
        PlayerAgent.SetDestination(transform.position);

        transform.LookAt(lookPoint);

        if (!previouslyShoot)
        {
            muzzleSpark.Play();
            audioSource.PlayOneShot(shootingSound);

            RaycastHit hit;
            if (Physics.Raycast(ShootingRaycastArea.transform.position, ShootingRaycastArea.transform.forward, out hit, shootingRadius))
            {
                Debug.Log("Shooting" + hit.transform.name);

                Enemy enemy = hit.transform.GetComponent<Enemy>();
                if (enemy != null)
                {
                    enemy.enemyHitDamage(giveDamage);
                }

                anim.SetBool("Running", false);
                anim.SetBool("Shooting", true);
            }
            previouslyShoot = true;
            Invoke(nameof(ActiveShooting), timebtwShoot);
        }
    }

    private void ActiveShooting()
    {
        previouslyShoot = false;
    }

    public void PlayerAIHitDamage(float takeDamage)
    {
        presentHealth -= takeDamage;

        if (presentHealth <= 0f)
        {
            StartCoroutine(Respawn());
        }
    }

    IEnumerator Respawn()
    {
        PlayerAgent.SetDestination(transform.position);
        PlayerSpeed = 0f;
        shootingRadius = 0f;
        visionRadius = 0f;
        enemyInvisionRadius = false;
        enemyInshootingRadius = false;
        anim.SetBool("Die", true);
        anim.SetBool("Running", false);
        anim.SetBool("Shooting", false);

        // animations

        Debug.Log("Dead");

        scoreManager.kills += 1;
        // khi playerAI bị chết thì sẽ tắt CapsuleCollider để lúc có bắn vào thì sẽ không tăng chỉ số điểm (kills)
        gameObject.GetComponent<CapsuleCollider>().enabled = false;

        yield return new WaitForSeconds(5f);

        Debug.Log("Spawn");
        // Khi playerAI sống lại thì lại bật CapsuleCollider lên
        gameObject.GetComponent<CapsuleCollider>().enabled = true;

        presentHealth = 120f;
        PlayerSpeed = 1f;
        shootingRadius = 10f;
        visionRadius = 100f;
        enemyInvisionRadius = true;
        enemyInshootingRadius = false;

        // animations
        anim.SetBool("Die", false);
        anim.SetBool("Running", true);

        // spawn point
        PlayerCharacter.transform.position = Spawn.transform.position;
        PursueEnemy();
    }
}
