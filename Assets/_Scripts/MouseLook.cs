﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    [Header("Min & Max camera view")]
    private const float Ymin = -50f;
    private const float Ymax = 50f;

    [Header("Camera view")]
    public Transform lookAt;
    public Transform player;

    [Header("Camera position")]
    public float CameraDistance = 10f;
    private float currentX = 0f;
    private float currentY = 0f;
    public float CameraSensitivity = 4f; // độ nhạy của camera

    public FloatingJoystick floatingJoystick;

    private void LateUpdate()
    {
        currentX += floatingJoystick.Horizontal * CameraSensitivity * Time.deltaTime;
        currentY -= floatingJoystick.Vertical * CameraSensitivity * Time.deltaTime;

        currentY = Mathf.Clamp(currentY, Ymin, Ymax); // giới hạn currentY từ Ymin đến Ymax

        // -CameraDistance trên trục Z. Điều này xác định khoảng cách từ đối tượng đến điểm nhìn.
        // Dấu "-" là vì khi muốn nhìn gần player hơn thì phải giảm số (dương), dấu trừ nó sẽ tính theo trục Z (mắt nhìn) và sẽ đến gần 0 (Player)
        Vector3 Direction = new Vector3(0, 0, -CameraDistance);

        Quaternion rotation = Quaternion.Euler(currentY, currentX, 0);

        // xác định vị trí mới của đối tượng dựa trên điểm nhìn và hướng quay.
        transform.position = lookAt.position + rotation * Direction;

        transform.LookAt(lookAt.position); // đảm bảo rằng đối tượng luôn hướng về điểm nhìn.
    }

}
