﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rifle : MonoBehaviour
{
    [Header("Rifle")]
    public Camera cam;
    public float giveDamage = 10f;
    public float shootingRange = 100f;
    public float fireCharge = 15f;
    public PlayerMovement player;
    public Animator animator;

    [Header("Rifle Ammunition and shooting")] //đạn súng trường khi bắn
    public float nextTimeToShoot = 0f;
    private int maximumAmmunition = 20;
    private int mag = 15;
    private int presentAmmunition;
    public float reloadingTime = 1.3f;
    private bool setReloading = false;

    [Header("Rifle Effects")]
    public ParticleSystem muzzleSpark;
    public GameObject woodedEffect;
    public GameObject goreEffect; // hiệu ứng máu

    [Header("Sound Effects")]
    public AudioSource audioSource;
    public AudioClip shootingSound;
    public AudioClip reloadingSound;

    private void Awake()
    {
        presentAmmunition = maximumAmmunition; // lượng đạn hiện tại = lượng đạn tối đa
    }

    public void Update()
    {
        if (setReloading) 
            return;

        if(presentAmmunition <= 0)
        {
            StartCoroutine(Reload());
            return;
        }

         /*
         if(player.mobileInputs == true)
         {
            if (Input.GetButton("Fire1") && Time.time >= nextTimeToShoot)
            {
                animator.SetBool("Fire", true);
                animator.SetBool("Idle", false);
                nextTimeToShoot = Time.time + 1f / fireCharge;

                Shoot();
            }

            else if (Input.GetButton("Fire1") && Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
            {
                animator.SetBool("Idle", false);
                animator.SetBool("FireWalk", true);
            }
            else if (Input.GetButton("Fire1") && Input.GetButton("Fire2"))
            {
                animator.SetBool("Idle", false);
                animator.SetBool("IdleAim", true);
                animator.SetBool("FireWalk", true);
                animator.SetBool("Walk", true);
                animator.SetBool("Reloading", false);
            }
            else
            {
                animator.SetBool("Fire", false);
                animator.SetBool("Idle", true);
                animator.SetBool("FireWalk", false);
            }
        }
        */

        //else
        if(player.mobileInputs == false)
        {
            if (Input.GetButton("Fire1") && Time.time >= nextTimeToShoot)
            {
                animator.SetBool("Fire", true);
                animator.SetBool("Idle", false);
                nextTimeToShoot = Time.time + 1f / fireCharge;

                Shoot();
            }

            else if (Input.GetButton("Fire1") && Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
            {
                animator.SetBool("Idle", false);
                animator.SetBool("FireWalk", true);
            }
            else if (Input.GetButton("Fire1") && Input.GetButton("Fire2"))
            {
                animator.SetBool("Idle", false);
                animator.SetBool("IdleAim", true);
                animator.SetBool("FireWalk", true);
                animator.SetBool("Walk", true);
                animator.SetBool("Reloading", false);
            }
            else
            {
                animator.SetBool("Fire", false);
                animator.SetBool("Idle", true);
                animator.SetBool("FireWalk", false);
            }
        }
    }


    public void mobileShoot()
    {
        if (Time.time >= nextTimeToShoot)
        {
            animator.SetBool("Fire", true);
            animator.SetBool("Idle", false);
            nextTimeToShoot = Time.time + 1f / fireCharge;

            Shoot();
        }
    }

    public void Shoot()
    {
        if(mag == 0) // nếu băng đạn = 0
        {
            // show amo out text
        }

        // Chạy đoạn code này trước rồi mới chạy cái if(mag == 0)
        presentAmmunition--; // Lượng đạn hiện tại trừ dần
        if(presentAmmunition == 0) // Nếu lượng đạn hiện tại = 0
        {
            mag--; // băng đạn trừ dần
        }

        // update UI
        AmmoCount.occurrence.UpdateAmmoText(presentAmmunition);
        AmmoCount.occurrence.UpdateMagText(mag);

        muzzleSpark.Play();
        audioSource.PlayOneShot(shootingSound);
        RaycastHit hitInfo;

        if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hitInfo, shootingRange))
        {
            Debug.Log(hitInfo.transform.name);

            // Nếu trỏ đến đối tượng nào có Component là Objects thì sẽ thực hiện các thuộc tính trong class Objects
            Objects objects = hitInfo.transform.GetComponent<Objects>();

            Enemy enemy = hitInfo.transform.GetComponent<Enemy>();

            if(objects != null) // Nếu đối tượng có chứa tập lệnh (class Objects)
            {
                objects.objectHitDamage(giveDamage);
                // Quaternion.LookRotation() - quay góc 1/4 và hướng về phía trước
                GameObject WoodGo = Instantiate(woodedEffect, hitInfo.point, Quaternion.LookRotation(hitInfo.normal));
                Destroy(WoodGo, 1f);
            }

            else if(enemy != null)
            {
                enemy.enemyHitDamage(giveDamage);
                GameObject goreGo = Instantiate(goreEffect, hitInfo.point, Quaternion.LookRotation(hitInfo.normal));
                Destroy(goreGo, 1f);
            }
        }
    }

    // Dừng một quá trình tại 1 thời điểm cụ thể
    // Dừnng thời gian khi các viên đạn = 0 thì chúng ta sẽ dừng việc bắn súng
    IEnumerator Reload()
    {
        player.playerSpeed = 0f; // dừng đi
        player.playerSprint = 0f; // dừng chạy
        setReloading = true;
        Debug.Log("Reloading........");
        // animation and audio
        animator.SetBool("Reloading", true);
        audioSource.PlayOneShot(reloadingSound);
        yield return new WaitForSeconds(reloadingTime); // thực hiện (chờ) 1 khoảng thời gian (reloadingTime) rồi mới chạy đoạn lệnh dưới
        // animations
        animator.SetBool("Reloading", false);
        presentAmmunition = maximumAmmunition; // sau khi nạp đạn xong thì lượng đạn hiện tại = lượng đạn max
        player.playerSpeed = 1.9f; // set lại tốc độ đi
        player.playerSprint = 3f; // set lại tốc độ chạy
        setReloading = false;
    }
}
